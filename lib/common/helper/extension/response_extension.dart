import 'package:dio/dio.dart';

extension ResponseExtension on Response {
  List<Map<String, dynamic>>? get listData {
    if (data == null) {
      return null;
    }

    if (data is! List<dynamic>) {
      return null;
    }

    final List<dynamic> list = data as List<dynamic>;

    return list.map((e) => e as Map<String, dynamic>).toList();
  }

  Map<String, dynamic>? get mapData {
    if (data == null) {
      return null;
    }

    if (data is! Map<String, dynamic>) {
      return null;
    }

    return data as Map<String, dynamic>;
  }
}