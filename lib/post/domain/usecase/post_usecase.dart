import 'package:sample_bloc/post/domain/exception/post_exception.dart';
import 'package:sample_bloc/post/domain/repository/post_repository_interface.dart';

import '../entity/post.dart';

class PostUseCase {
  final PostRepositoryInterface repository;

  const PostUseCase(this.repository);

  Future<List<Post>> fetchPosts() async {
    return await repository.fetchPosts();
  }

  Future<Post> fetchPost(String id) async {
    return await repository.fetchPost(id);
  }

  Future<void> addPost(Post post) async {

    if (post.title.isEmpty || post.body.isEmpty) {
      throw const PostInvalidFieldsException();
    }

    await repository.addPost(post);
  }

  Future<void> deletePost(String id) async {
    await repository.deletePost(id);
  }
}