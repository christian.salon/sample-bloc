import '../entity/post.dart';

abstract interface class PostRepositoryInterface {
  /// Fetches posts
  Future<List<Post>> fetchPosts();

  /// Fetches a post by id
  /// [id] is the id of the post
  /// Returns a [Post] if found, otherwise throws an [Exception]
  Future<Post> fetchPost(String id);

  /// Adds a post
  /// [post] is the post to add
  /// Returns a [Post] if added, otherwise throws an [Exception]
  Future<Post> addPost(Post post);

  /// Deletes a post
  /// [id] is the id of the post to delete
  Future<void> deletePost(String id);
}