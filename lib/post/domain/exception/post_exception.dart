sealed class PostException implements Exception {
  const PostException(this.message);

  final String message;

  @override
  String toString() => message;
}

/// Thrown when a post is not found
class PostNotFoundException extends PostException {
  const PostNotFoundException() : super('Post not found');
}

/// Thrown when a post has invalid fields
class PostInvalidFieldsException extends PostException {
  const PostInvalidFieldsException() : super('Post has invalid fields');
}
