import 'package:sample_bloc/post/domain/entity/post.dart';

class PostModel extends Post {
  const PostModel({
    required int id,
    required String title,
    required String body,
  }) : super(
          id: id,
          title: title,
          body: body,
        );

  factory PostModel.fromJson(Map<String, dynamic> json) {
    return PostModel(
      id: json['id'] as int? ?? -1,
      title: json['title'] as String? ?? '',
      body: json['body'] as String? ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'body': body,
    };
  }
}
