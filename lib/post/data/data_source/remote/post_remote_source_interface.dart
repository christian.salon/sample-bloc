import '../../model/output/post_model.dart';

abstract interface class PostRemoteSourceInterface {
  Future<List<PostModel>> fetchPosts();
}