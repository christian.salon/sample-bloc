import 'dart:io';

import 'package:dio/dio.dart';
import 'package:sample_bloc/common/helper/extension/response_extension.dart';

import '../../model/output/post_model.dart';
import 'post_remote_source_interface.dart';

class PostRemoteSource implements PostRemoteSourceInterface {
  final Dio dio;

  const PostRemoteSource(this.dio);

  /// Note: It is recommended to put the base URL in an environment variable file and use it here. But for simplicity, we will put it here.
  static const String _baseUrl = 'https://jsonplaceholder.typicode.com';

  @override
  Future<List<PostModel>> fetchPosts() async {
    final Response response = await dio.get('$_baseUrl/posts');

    if (response.statusCode != HttpStatus.ok) {
      /// Note: You can also use your own custom exception instead of [Exception].
      throw Exception('Error fetching posts');
    }

    final data = response.listData ?? [];

    return data.map((post) => PostModel.fromJson(post)).toList();
  }
}
