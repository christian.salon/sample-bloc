import '../../domain/entity/post.dart';
import '../../domain/repository/post_repository_interface.dart';
import '../data_source/remote/post_remote_source_interface.dart';

class PostRepository implements PostRepositoryInterface {
  final PostRemoteSourceInterface remoteSource;

  const PostRepository(this.remoteSource);

  @override
  Future<Post> addPost(Post post) {
    // TODO: implement addPost
    throw UnimplementedError();
  }

  @override
  Future<void> deletePost(String id) {
    // TODO: implement deletePost
    throw UnimplementedError();
  }

  @override
  Future<Post> fetchPost(String id) {
    // TODO: implement fetchPost
    throw UnimplementedError();
  }

  @override
  Future<List<Post>> fetchPosts() async {
    return await remoteSource.fetchPosts();
  }
}