import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../di/post_service_locator.dart';
import '../component/post_list_view.dart';
import '../event/post_event.dart';
import '../state/posts_state.dart';
import '../viewmodel/posts_viewmodel.dart';

class PostsScreen extends StatelessWidget {
  const PostsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => PostsViewModel(postUseCase)..add(const PostsRequested()),
      child: const _PostsScreen(),
    );
  }
}

class _PostsScreen extends StatelessWidget {
  const _PostsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Posts Display')),
      body: BlocBuilder<PostsViewModel, PostsState>(
        builder: (context, state) {
          if (state.status.isLoading || state.status.isInitial) {
            return const Center(child: CircularProgressIndicator());
          } else if (state.status.isSuccess) {
            return PostListView(post: state.posts);
          } else if (state.status.isError) {
            return Center(child: Text(state.error.toString()));
          } else {
            return const SizedBox.shrink();
          }
        },
      ),
    );
  }
}
