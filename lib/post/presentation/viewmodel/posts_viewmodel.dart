import 'package:flutter_bloc/flutter_bloc.dart';

import '../../domain/usecase/post_usecase.dart';
import '../event/post_event.dart';
import '../state/posts_state.dart';

/// Note: If using Bloc as a View Model, it is recommended to make all methods private and only expose events.
class PostsViewModel extends Bloc<PostEvent, PostsState> {
  final PostUseCase _postUseCase;

  PostsViewModel(this._postUseCase) : super(const PostsState()) {
    on<PostsRequested>(_fetchPosts);
  }

  Future<void> _fetchPosts(
    PostsRequested event,
    Emitter<PostsState> emit,
  ) async {
    try {
      emit(state.copyWith(status: PostsStatus.loading));

      final posts = await _postUseCase.fetchPosts();

      emit(state.copyWith(status: PostsStatus.success, posts: posts));
    } on Exception catch (error) {
      emit(state.copyWith(status: PostsStatus.error, error: error));
    }
  }
}
