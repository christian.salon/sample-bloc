import 'package:equatable/equatable.dart';

import '../../domain/entity/post.dart';

enum PostsStatus {
  initial,
  loading,
  success,
  error;

  bool get isInitial => this == PostsStatus.initial;
  bool get isLoading => this == PostsStatus.loading;
  bool get isSuccess => this == PostsStatus.success;
  bool get isError => this == PostsStatus.error;
}

class PostsState extends Equatable {
  const PostsState({
    this.status = PostsStatus.initial,
    this.posts = const <Post>[],
    this.error,
  });

  final PostsStatus status;
  final List<Post> posts;
  final Exception? error;

  PostsState copyWith({
    PostsStatus? status,
    List<Post>? posts,
    bool? hasReachedMax,
    Exception? error,
  }) {
    return PostsState(
      status: status ?? this.status,
      posts: posts ?? this.posts,
      error: error,
    );
  }

  @override
  List<Object?> get props => [status, posts, error];
}