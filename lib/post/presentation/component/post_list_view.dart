import 'package:flutter/material.dart';
import 'package:sample_bloc/post/presentation/component/post_card.dart';

import '../../domain/entity/post.dart';

class PostListView extends StatelessWidget {
  final List<Post> post;

  const PostListView({Key? key, required this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: post.length,
      itemBuilder: (context, index) {
        return PostCard(
          uiState: PostCardUiState(
            id: post[index].id,
            title: post[index].title,
            titleStyle: const TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
            body: post[index].body,
            bodyStyle: const TextStyle(
              fontSize: 16,
            ),
          ),
        );
      },
    );
  }
}
