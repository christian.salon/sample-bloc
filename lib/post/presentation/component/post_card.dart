import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class PostCard extends StatelessWidget {
  const PostCard({
    Key? key,
    required this.uiState,
  }) : super(key: key);

  final PostCardUiState uiState;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(
          uiState.title,
          style: uiState.titleStyle,
        ),
        subtitle: Text(
          uiState.body,
          style: uiState.bodyStyle,
        ),
      ),
    );
  }
}

class PostCardUiState extends Equatable {
  const PostCardUiState({
    this.id = 0,
    this.title = '',
    this.body = '',
    this.titleStyle = const TextStyle(),
    this.bodyStyle = const TextStyle(),
  });

  final int id;
  final String title;
  final TextStyle titleStyle;
  final String body;
  final TextStyle bodyStyle;

  @override
  List<Object?> get props => [id, title];
}