import 'package:equatable/equatable.dart';

sealed class PostEvent extends Equatable {
  const PostEvent();
}

class PostsRequested extends PostEvent {
  const PostsRequested();

  @override
  List<Object?> get props => [];
}