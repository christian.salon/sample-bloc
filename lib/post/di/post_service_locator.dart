import '../../common/di/http_service_locator.dart';
import '../data/data_source/remote/post_remote_source.dart';
import '../data/repository/post_repository.dart';
import '../domain/usecase/post_usecase.dart';

final postRemoteSource = PostRemoteSource(dio);
final postRepository = PostRepository(postRemoteSource);
final postUseCase = PostUseCase(postRepository);