import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:sample_bloc/common/helper/extension/response_extension.dart';

void main() {
  group('ResponseExtension', () {
    group('listData', () {
      test('should return null when data is null', () {
        final response = Response(
          data: null,
          statusCode: 200,
          requestOptions: RequestOptions(path: ''),
        );

        expect(response.listData, null);
      });

      test('should return null when data is not a list', () {
        final response = Response(
          data: {},
          statusCode: 200,
          requestOptions: RequestOptions(path: ''),
        );

        expect(response.listData, null);
      });

      test('should return list when data is a list', () {
        final response = Response(
          data: [],
          statusCode: 200,
          requestOptions: RequestOptions(path: ''),
        );

        expect(response.listData, []);
      });
    });

    group('mapData', () {
      test('should return null when data is null', () {
        final response = Response(
          data: null,
          statusCode: 200,
          requestOptions: RequestOptions(path: ''),
        );

        expect(response.mapData, null);
      });

      test('should return null when data is not a map', () {
        final response = Response(
          data: [],
          statusCode: 200,
          requestOptions: RequestOptions(path: ''),
        );

        expect(response.mapData, null);
      });

      test('should return map when data is a map', () {
        final response = Response(
          data: <String, dynamic>{},
          statusCode: 200,
          requestOptions: RequestOptions(path: ''),
        );

        expect(response.mapData, {});
      });
    });
  });
}
