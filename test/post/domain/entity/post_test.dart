import 'package:flutter_test/flutter_test.dart';
import 'package:sample_bloc/post/domain/entity/post.dart';

void main() {
  test('PostEntity should be created', () {
    const postEntity = Post(
      id: 1,
      title: 'title',
      body: 'body',
    );

    // assert
    expect(postEntity.id, 1);
    expect(postEntity.title, 'title');
    expect(postEntity.body, 'body');
  });
}