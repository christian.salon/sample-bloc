import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sample_bloc/post/domain/entity/post.dart';
import 'package:sample_bloc/post/domain/exception/post_exception.dart';
import 'package:sample_bloc/post/domain/repository/post_repository_interface.dart';
import 'package:sample_bloc/post/domain/usecase/post_usecase.dart';

class _MockPostRepository extends Mock implements PostRepositoryInterface {}

void main() {
  group('PostUseCase', () {
    late _MockPostRepository mockPostRepository;
    late PostUseCase postUseCase;

    setUp(() {
      mockPostRepository = _MockPostRepository();
      postUseCase = PostUseCase(mockPostRepository);
    });

    group('Fetch Posts', () {
      test('SHOULD fetch posts', () async {
        // arrange
        when(() => mockPostRepository.fetchPosts()).thenAnswer((_) async => []);
        // act
        await postUseCase.fetchPosts();
        // assert
        verify(() => mockPostRepository.fetchPosts());
      });
    });

    group('Fetch a post', () {
      test('should fetch a post', () async {
        // arrange
        when(() => mockPostRepository.fetchPost('1')).thenAnswer(
                (_) async => const Post(id: 1, title: 'title', body: 'body'));
        // act
        final post = await postUseCase.fetchPost('1');
        // assert
        expect(post, const Post(id: 1, title: 'title', body: 'body'));
        verify(() => mockPostRepository.fetchPost('1'));
      });
    });

    group('Add post', () {
      test('SHOULD add a post successfully', () async {
        // arrange
        when(() => mockPostRepository
            .addPost(const Post(id: 1, title: 'title', body: 'body')))
            .thenAnswer(
                (_) async => const Post(id: 1, title: 'title', body: 'body'));
        // act
        await postUseCase
            .addPost(const Post(id: 1, title: 'title', body: 'body'));
        // assert
        verify(() => mockPostRepository
            .addPost(const Post(id: 1, title: 'title', body: 'body')));
      });

      test('SHOULD throw PostInvalidFieldsException WHEN some fields are empty', () async {
        // act
        final call = postUseCase
            .addPost(const Post(id: 1, title: '', body: 'body'));
        // assert
        expect(() => call, throwsA(const PostInvalidFieldsException()));
      });
    });

    group('Delete post', () {
      test('SHOULD delete a post successfully', () async {
        // arrange
        when(() => mockPostRepository.deletePost('1')).thenAnswer((_) async {});
        // act
        await postUseCase.deletePost('1');
        // assert
        verify(() => mockPostRepository.deletePost('1'));
      });
    });
  });
}
