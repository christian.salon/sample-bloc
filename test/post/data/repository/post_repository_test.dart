import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sample_bloc/post/data/data_source/remote/post_remote_source_interface.dart';

class _MockPostRemoteSource extends Mock implements PostRemoteSourceInterface {}

void main() {
  group('PostRepository', () {
    late _MockPostRemoteSource mockRemoteSource;

    setUp(() {
      mockRemoteSource = _MockPostRemoteSource();
    });

    group('Fetch Posts', () {
      test('should fetch posts', () async {
        // arrange
        when(() => mockRemoteSource.fetchPosts()).thenAnswer((_) async => []);

        // act
        await mockRemoteSource.fetchPosts();

        // assert
        verify(() => mockRemoteSource.fetchPosts()).called(1);
      });
    });
  });
}
