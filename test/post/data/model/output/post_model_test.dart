import 'package:flutter_test/flutter_test.dart';
import 'package:sample_bloc/post/data/model/output/post_model.dart';

void main() {
  group('PostModel', () {
    test('should be created', () {
      // arrange
      const postModel = PostModel(
        id: 1,
        title: 'title',
        body: 'body',
      );

      // assert
      expect(postModel.id, 1);
      expect(postModel.title, 'title');
      expect(postModel.body, 'body');
    });

    test('should be created from json', () {
      // arrange
      const postModel = PostModel(
        id: 1,
        title: 'title',
        body: 'body',
      );

      // act
      final postModelFromJson = PostModel.fromJson(const {
        'id': 1,
        'title': 'title',
        'body': 'body',
      });

      // assert
      expect(postModelFromJson, postModel);
    });

    test('should be converted to json', () {
      // arrange
      const postModel = PostModel(
        id: 1,
        title: 'title',
        body: 'body',
      );

      // act
      final postModelToJson = postModel.toJson();

      // assert
      expect(postModelToJson, {
        'id': 1,
        'title': 'title',
        'body': 'body',
      });
    });
  });
}