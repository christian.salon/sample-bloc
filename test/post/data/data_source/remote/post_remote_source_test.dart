import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sample_bloc/post/data/data_source/remote/post_remote_source.dart';

class _MockDio extends Mock implements Dio {}

void main() {
  group('PostRemoteSource', () {
    late _MockDio mockDio;
    late PostRemoteSource postRemoteSource;

    setUp(() {
      mockDio = _MockDio();
      postRemoteSource = PostRemoteSource(mockDio);
    });

    test('should return Dio instance', () {
      // arrange
      when(() => mockDio.options).thenReturn(BaseOptions());
      // act
      final dio = mockDio;
      // assert
      expect(dio, isA<Dio>());
    });

    group('SHOULD fetch posts', () {
      test('SHOULD return a list of posts', () async {
        // arrange
        when(() => mockDio.get(any())).thenAnswer((_) async => Response(
              data: [
                {
                  'id': 1,
                  'title': 'title',
                  'body': 'body',
                },
                {
                  'id': 2,
                  'title': 'title',
                  'body': 'body',
                },
              ],
              statusCode: 200,
              requestOptions: RequestOptions(path: ''),
            ));
        // act
        final posts = await postRemoteSource.fetchPosts();
        // assert
        expect(posts.length, 2);
      });

      test('SHOULD throw exception if response status is not OK', () async {
        // arrange
        when(() => mockDio.get(any())).thenAnswer((_) async => Response(
              data: [],
              statusCode: 400,
              requestOptions: RequestOptions(path: ''),
            ));
        try {
          // act
          await postRemoteSource.fetchPosts();
        } catch (e) {
          // assert
          expect(e, isA<Exception>());
        }
      });
    });
  });
}
