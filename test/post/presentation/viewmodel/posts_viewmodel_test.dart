import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sample_bloc/post/domain/entity/post.dart';
import 'package:sample_bloc/post/domain/usecase/post_usecase.dart';
import 'package:sample_bloc/post/presentation/event/post_event.dart';
import 'package:sample_bloc/post/presentation/state/posts_state.dart';
import 'package:sample_bloc/post/presentation/viewmodel/posts_viewmodel.dart';

class _MockPostUseCase extends Mock implements PostUseCase {}

void main() {
  group('PostsViewModel', () {
    late _MockPostUseCase mockPostUseCase;
    late PostsViewModel postsViewModel;

    setUp(() {
      mockPostUseCase = _MockPostUseCase();
      postsViewModel = PostsViewModel(mockPostUseCase);
    });

    group('Fetch Posts', () {
      final posts = [
        const Post(id: 1, title: 'title1', body: 'body1'),
        const Post(id: 2, title: 'title2', body: 'body2'),
      ];

      blocTest(
        'SHOULD fetch posts successfully',
        build: () {
          when(() => mockPostUseCase.fetchPosts())
              .thenAnswer((_) async => posts);
          return postsViewModel;
        },
        act: (PostsViewModel bloc) => bloc.add(const PostsRequested()),
        expect: () => [
          const PostsState(status: PostsStatus.loading),
          PostsState(status: PostsStatus.success, posts: posts),
        ],
      );

      final error = Exception();

      blocTest(
        'SHOULD update state to error',
        build: () {
          when(() => mockPostUseCase.fetchPosts()).thenThrow(error);
          return postsViewModel;
        },
        act: (PostsViewModel bloc) => bloc.add(const PostsRequested()),
        expect: () => [
          const PostsState(status: PostsStatus.loading),
          PostsState(status: PostsStatus.error, error: error),
        ],
      );
    });
  });
}
