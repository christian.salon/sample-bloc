import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:sample_bloc/post/presentation/component/post_card.dart';

void main() {
  group('PostCard', () {
    testWidgets('SHOULD render correctly', (WidgetTester tester) async {
      // arrange
      await tester.pumpWidget(const MaterialApp(
        home: PostCard(
          uiState: PostCardUiState(
            id: 1,
            title: 'title',
            body: 'body',
          ),
        ),
      ));
      // assert
      expect(find.text('title'), findsOneWidget);
      expect(find.text('body'), findsOneWidget);
    });
  });
}
