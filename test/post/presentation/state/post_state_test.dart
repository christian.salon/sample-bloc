import 'package:flutter_test/flutter_test.dart';
import 'package:sample_bloc/post/presentation/state/posts_state.dart';

void main() {
  group('PostState', () {
    test('SHOULD be initial', () {
      const postState = PostsState();

      expect(postState.status, PostsStatus.initial);
      expect(postState.posts, []);
      expect(postState.error, null);
    });

    test('SHOULD be loading', () {
      const postState = PostsState(status: PostsStatus.loading);

      expect(postState.status, PostsStatus.loading);
      expect(postState.posts, []);
      expect(postState.error, null);
    });

    test('SHOULD be success', () {
      const postState = PostsState(status: PostsStatus.success);

      expect(postState.status, PostsStatus.success);
      expect(postState.posts, []);
      expect(postState.error, null);
    });

    test('SHOULD be error', () {
      const postState = PostsState(status: PostsStatus.error);

      expect(postState.status, PostsStatus.error);
      expect(postState.posts, []);
      expect(postState.error, null);
    });

    test('SHOULD update using copyWith', () {
      const postState = PostsState(
        status: PostsStatus.loading,
        posts: [],
        error: null,
      );

      final updatedPostState = postState.copyWith(
        status: PostsStatus.success,
        posts: const [],
        hasReachedMax: false,
        error: null,
      );

      expect(updatedPostState.status, PostsStatus.success);
      expect(updatedPostState.posts, []);
      expect(updatedPostState.error, null);
    });
  });
}